// Khymari Sandy - 2235225

package application;

import vehicles.Bicycle;

public class BikeStore
{
    public static void main(String[] args)
    {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("man1", 5, 20);
        bikes[1] = new Bicycle("man2", 6, 25);
        bikes[2] = new Bicycle("man3", 7, 30);
        bikes[3] = new Bicycle("man4", 8, 35);

        for( Bicycle b : bikes)
        {
            System.out.println(b);
        }
    }
}
