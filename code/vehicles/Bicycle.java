// Khymari Sandy - 2235225

package vehicles;

public class Bicycle 
{
    String manufacturer;
    int numberGears;
    double maxSpeed;

    public Bicycle(String man, int gear, double speed)
    {
        this.manufacturer = man;
        this.numberGears = gear;
        this.maxSpeed = speed;
    }

    public String getManufacturer()
    {
        return this.manufacturer;
    }

    public int getGears()
    {
        return this.numberGears;
    }

    public double getSpeed()
    {
        return this.maxSpeed;
    }

    public String toString()
    {
        return String.format("Manufacuturer: %s\nNumber of Gears: %s\nMax Speed: %s", 
                                    this.manufacturer, this.numberGears, this.maxSpeed);
    }
}
